# pokedex Pragma

Test project with vanilla js. Making a pokedex

![Project img](https://gitlab.com/carlos.garciatru/pokedex/-/raw/main/src/images/project_img.png)

Project demo: [https://pragma-pokedex.netlify.app/](https://pragma-pokedex.netlify.app/)