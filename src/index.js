let pokemons = [
  {
    id: 1,
    name: 'CHARIZARD',
    img: '/src/images/charizard.png',
    typeIcon: '/src/images/fire.png',
    background: '#f7a464',
    data: {
      no: '006',
      level: 100,
      type: 'Fire',
      skill: 'Flames',
      height: '1,7 m',
      weight: '90,5 Kg'
    },
  },
  {
    id: 2,
    name: 'CHIKORITA',
    img: '/src/images/chikorita.png',
    background: '#96ce38',
    typeIcon: '/src/images/leaf.png',
    data: {
      no: '006',
      level: 100,
      type: 'Grass',
      skill: 'Leaf Guard',
      height: '1 m',
      weight: '10,5 Kg'
    },
  },
  {
    id: 3,
    name: 'PIKACHU',
    img: '/src/images/Pikachu.png',
    background: '#f2c827',
    typeIcon: '/src/images/lightning.png',
    data: {
      no: '006',
      level: 100,
      type: 'Electric',
      skill: 'Lightning',
      height: '1,5 m',
      weight: '9,5 Kg'
    },
  },
  {
    id: 4,
    name: 'SQUIRTLE',
    img: '/src/images/squirtle.png',
    background: '#aed5d6',
    typeIcon: '/src/images/drop.png',
    data: {
      no: '006',
      level: 100,
      type: 'Water',
      skill: 'Rain Dish',
      height: '8,7 m',
      weight: '20,5 Kg'
    },
  }
]

let selectedPokemon = pokemons[0];

const onClickPokemon = () => {

  const main = document.getElementById('main');
  main.textContent = '';
  const {
    name,
    img,
    background,
    typeIcon,
    data,
  } = selectedPokemon;

  let information = '';

  Object.entries(data).forEach(item => {
    information += `
      <div class="pokemon__item">
        <p class="pokemon__info-title">${item[0]}</p>
        <span class="pokemon__info-desc">${item[1]}</span>
      </div>
    `
  })
  main.style.backgroundColor = background;
  main.innerHTML = `
    <section class="pokemon">
      <div class="pokemon__container">
        <figure class="pokemon__title">
          <img src="${typeIcon}" alt="${name} element" class="pokemon__element">
          <figcaption class="pokemon__name">${name}</figcaption>
        </figure>
      </div>
      <figure class="pokemon__fig">
        <img src="${img}" alt="${name} pokemon" class="pokemon__img">
      </figure>
    </section>
    <section class="pokemon-info">
      ${information}
    </section>
  `
}

const createFooterCarrousel = () => {
  const footerCarrousel = document.getElementById('carrousel');
  pokemons.forEach(({img,id,background}) => {
    const pokemonItem = document.createElement('div');
    pokemonItem.innerHTML = `
    <figure class="footer__item" >
      <img src="${img}" alt="pokemon" class="footer__img"/>
    </figure>
    `

    pokemonItem.firstElementChild.style.backgroundColor = background

    pokemonItem.firstElementChild.addEventListener('click', (e) => {
      selectedPokemon = pokemons.find(pok => pok.id === id);
      onClickPokemon();
    })

    footerCarrousel.appendChild(pokemonItem.firstElementChild)
  })
}

onClickPokemon()
createFooterCarrousel()

